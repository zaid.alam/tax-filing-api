const express = require('express')
const router = express.Router()
const newTaxPayer = require('../controllers/newTaxPayer')
const getTaxPayer = require('../controllers/getTaxPayer')
const updateTaxPayer = require('../controllers/updateTaxPayer')

router.route('/')
    .post(newTaxPayer)
    .get(getTaxPayer)
    .put(updateTaxPayer)

module.exports = router