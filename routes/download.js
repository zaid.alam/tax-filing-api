const express = require('express')
const router = express.Router();
const downloadReport = require('../controllers/downloadReport')

router.route('/')
    .get(downloadReport)

module.exports = router