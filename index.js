const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
mongoose.connect('mongodb://mongo:27017/taxDB', {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

const taxPayer = require('./routes/taxPayer')
const download = require('./routes/download')

app = express()
app.use(express.json())
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Headers', 
    'Origin, X-requested-With, Content-Type, Accept, Authorization')

    if(req.method === 'OPTIONS'){
        res.header('Access-Control-Allow-Methods', 
        'PUT, POST, PATCH, GET, DELETE')
        res.status(200).json({})
    }
    next()
})


app.use('/taxpayer', taxPayer)
app.use('/download', download);

app.use((req, res, next) => {
    const error = new Error("Invalid API Request")
    error.status = 404
    next(error)
})
app.use((error, req, res, next) => {
    res.status(error.status || 500).json({
        error:{
            message: error.message
        }
    })
})

app.listen(process.env.PORT || 8000, () => {
    console.log("Server is up and running");
})