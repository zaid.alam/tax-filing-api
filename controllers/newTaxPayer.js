const taxPayer = require('../models/taxPayers')
const xmljs = require('xml2js')

module.exports = async (req, res, next) => {
    let flag = false
    await taxPayer.findById(req.body["tax-filing"].pan, (err, response) => {
        if(err){
            res.status(500).json({
                error: err
            })
        }else{
            if(response){
                res.status(400).json({
                    error: "Taxpayer is already present"
                })
                flag = true
            }
        }
    })

    if(flag)
        return

    console.log(req.body);
    const id = req.body["tax-filing"].pan
    console.log(id);
    const builder = new xmljs.Builder()
    const xmlResponse = builder.buildObject(req.body)
    const data = Buffer.from(xmlResponse).toString('base64')
    console.log(data);
    console.log(Buffer.from(data, 'base64').toString())

    taxPayer({"_id": id, "data": data}).save()
        .then((result) => {
            res.status(201).json({
                "msg":"Saved Successfully"
            })
        }).catch((err) => {
            res.status(500).json({
                "error": err
            })
        });


}