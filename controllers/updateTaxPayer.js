const taxPayer = require('../models/taxPayers')
const xmljs = require("xml2js")
const {Base64} = require('js-base64')

module.exports = async (req, res, next) => {

    let flag = false
    
    await taxPayer.findById(req.body["tax-filing"].pan, (err, result) => {
        if(err){
            res.status(500).json({
                error: err
            })
        }else{
            if(!result){
                res.status(400).json({
                    error: "This User does not exist"
                })
                flag = true
            }
        }
    })

    if(flag)
        return

    const id = req.body["tax-filing"].pan
    console.log(id);
    const builder = new xmljs.Builder()
    const xmlResponse = builder.buildObject(req.body)
    const data = Buffer.from(xmlResponse).toString('base64')
    
    await taxPayer.findByIdAndUpdate(id, {"data": data}, (err, result)=>{
        if(err){
            res.status(500).json({
                error: err
            })
        }else{
            res.status(201).json({
                "msg": "Data Updated Successfully"
            })
        }
    })
}