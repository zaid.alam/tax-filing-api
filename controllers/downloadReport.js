const taxPayer = require("../models/taxPayers");
const libxml = require('libxmljs')
const fs = require('fs')

module.exports = async (req, res, next) => {
    // console.log(req);
    await taxPayer.findById(req.query.pan, (err, response) => {
        if (!err) {
            if (!response) {
                res.status(400).json({
                    error: "no such user exists"
                })
            } else {
                // console.log(response);
                const x = response.data
                const xmlObject = Buffer.from(x, 'base64').toString()

                const xmlDoc = libxml.parseXml(xmlObject)

                fs.writeFileSync(__dirname + '\\' + req.query.pan + '.xml', xmlDoc.toString())

                res.download(__dirname + '\\' + req.query.pan + '.xml', err => {
                    console.log("file sent");
                })              
            }
        } else {
            res.status(500).json({
                error: err
            });
        }
    });
        
    
};
