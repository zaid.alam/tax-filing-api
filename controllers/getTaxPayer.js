const taxPayer = require("../models/taxPayers");
const libxml = require('libxmljs')

module.exports = async (req, res, next) => {
    // console.log(req);
    taxPayer.findById(req.query.pan, (err, response) => {
        if (!err) {
            if(!response){
                res.status(400).json({
                    error: "no such user exists"
                })
            }else{
            // console.log(response);
            const x = response.data
            const xmlObject = Buffer.from(x, 'base64').toString()
            console.log(typeof(xmlObject));
            
            const xmlDoc = libxml.parseXml(xmlObject)
            console.log(typeof(xmlDoc.get('//investments')));

            const result = {
                "tax-filing":{
                    "name": xmlDoc.get('//name').text(),
                    "email": xmlDoc.get('//email').text(),
                    "pan": xmlDoc.get('//pan').text(),
                    "investments":{
                        "ppf": xmlDoc.get('//ppf').text(),
                        "mutual-funds": xmlDoc.get('//mutual-funds').text(),
                        "fixed-deposit": xmlDoc.get('//fixed-deposit').text(),
                        "home-loan-principal": xmlDoc.get('//home-loan-principal').text(),
                        "insurance": xmlDoc.get('//insurance').text(),
                        "medical-insurance": xmlDoc.get('//medical-insurance').text()
                    }
                }

            }

            console.log(result);
            res.status(200).json(result)
            
        }
        } else {
            res.status(500).json({
                error: err
            });
        }
    });
};
