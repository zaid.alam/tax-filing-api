const mongoose = require('mongoose');

const taxPayerSchema = mongoose.Schema({
    "_id":{
        type: String,
        required: true
    },
    "data": {
        type: String,
        required: true
    }
})

module.exports = new mongoose.model("TaxPayer", taxPayerSchema);